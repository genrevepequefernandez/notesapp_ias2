// = VALIDATION AND RESPONSE
export default {
 data() {
     return {
         isLoading: false,
         initialLoading: false,
     }
 },
 methods: {
     UnprocEntity(data) {
         for (const key of Object.keys(data)) {
             this.$toast.error(data[key][0]);
         }
         this.isLoading = false
     },
     successResponse(data) {
         this.isLoading = false
         return this.$toast.success(data.msg)
     },
     errResponse(data) {
         this.isLoading = false
         return this.$toast.error(data.msg)
     },
 }
}