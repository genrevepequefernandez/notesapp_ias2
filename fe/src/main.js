import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify'
import store from './store'
import response from './assets/js/response'
import Toast from "vue-toastification";

import "vue-toastification/dist/index.css";

Vue.config.productionTip = false

Vue.mixin(response)

Vue.use(Toast, {
  position: "top-center",
  timeout: 2000,
  closeOnClick: true,
  pauseOnFocusLoss: true,
  pauseOnHover: true,
  showCloseButtonOnHover: false,
  hideProgressBar: true,
  closeButton: false,
  icon: true,
  rtl: false,
  maxToasts: 2,
  transition: "Vue-Toastification__fade",
  newestOnTop: true
});

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
