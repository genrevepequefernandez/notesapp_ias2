import API from '../base/'

export default {
  namespaced: true,
  state: {
    notes: []
  },
  getters: {
   GET_NOTES(state){
    return state.notes
   }
  },
  mutations: {
    SET_USER_NOTES(state, data) {
      state.notes = data
    },
  },
  actions: {
    async get({commit}, data){
      const res = await API.get(`notes?search=${data}`, data).then(res => {
        commit('SET_USER_NOTES', res.data)
        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async store({commit}, data){
      const res = await API.post(`notes`, data).then(res => {
        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async update({commit}, data){
     const res = await API.put(`notes/${data.id}`, data).then(res => {
        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async updateOrder({commit}, data){
     const res = await API.post(`notes/updateOrder`, {notes: data}).then(res => {
        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async delete({commit}, data){
     const res = await API.delete(`notes/${data.id}`).then(res => {
        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
  },
}