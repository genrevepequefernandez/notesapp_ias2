import API from '../base/'

export default {
  namespaced: true,
  state: {
    user: {
      name: '',
      info: {
        first_name: "Evsu",
        last_name: "Tacloban",
      }
    },
    userinfo: [],
    useraccount: [],
    signup: '',
    adminlogs: [],
    userlogs: [],
    accounts: [],
    token: localStorage.getItem('auth') || '',
    selectedAccount: [],
  },
  getters: {
    GET_USER(state) {
      return state.user;
    }
  },
  mutations: {
    SET_USER_ACC(state, data) {
      state.user = data
    },
    SET_USER_TOKEN(state, token) {
     localStorage.setItem('auth', token)
     localStorage.setItem('isUser', 'true')
     state.token = token

     const bearer_token = localStorage.getItem('auth') || ''
     API.defaults.headers.common['Authorization'] = `Bearer ${bearer_token}`
    },
    SET_AUTH_TOKEN(state, token) {
     localStorage.setItem('auth', token)
     localStorage.setItem('isAdmin', 'true')
     state.token = token

     const bearer_token = localStorage.getItem('auth') || ''
     API.defaults.headers.common['Authorization'] = `Bearer ${bearer_token}`
    },
    UNSET_USER(state){
     localStorage.removeItem('auth');
     localStorage.removeItem('isUser');
     state.token = ''
     state.user = {

      },

     API.defaults.headers.common['Authorization'] = ''
   } 
  },
  actions: {
    async login({commit}, payload){
      const res = await API.post('/auth/login', payload).then(res => {
        commit('SET_USER_ACC', res.data.user)
        commit('SET_USER_TOKEN', res.data.access_token)

        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async register({commit}, payload){
      const res = await API.post('/auth/store', payload).then(res => {

        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async update({commit}, data){
      const res = await API.post(`auth/update`, data).then(res => {
        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
   
    async logout({commit}){
     const res = await API.post('auth/logout?token=' + localStorage.getItem('auth')).then(response => {
       commit('UNSET_USER')
       return response
     }).catch(error => {
       return error.response
     });

     return res;
   },
   
   async changeUserPassword({commit}, data) {
    const res = await API.post('auth/user/change_password', data).then(res => {
      return res;
    }).catch(error => {
      return error.response;
    })

    return res;
   },
   async checkAuthUser({commit}) {
    const res = await API.post('auth/me?token=' + localStorage.getItem('auth')).then(res => {
      commit('SET_USER_ACC', res.data.user)

      return res;
    }).catch(error => {
      return error.response;
    })

    return res;
   },
   
  },
}