<?php

use App\Http\Controllers\NotesController;
use App\Http\Controllers\UserAuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {

    Route::post('login', [UserAuthController::class, 'login']);
    Route::post('store', [UserAuthController::class, 'store']);
    Route::post('logout', [UserAuthController::class, 'logout']);
    Route::post('refresh', [UserAuthController::class, 'refresh']);
    Route::post('me', [UserAuthController::class, 'me']);
});

Route::group(['middleware' => 'api'], function () {
    Route::post('notes/updateOrder', [NotesController::class, 'updateOrder']);
    Route::apiResource('notes', NotesController::class);
});