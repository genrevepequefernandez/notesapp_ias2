<?php

namespace App\Http\Controllers;

use App\Models\Note;
use Illuminate\Http\Request;

class NotesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(Request $request)
    {
        return response()->json(Note::where('title', 'like', '%'.$request->search.'%')->where('user_id', auth()->user()->id)->orderBy('order')->get());
    }

    public function store(Request $request)
    {
        $note = Note::where('user_id', auth()->user()->id)->latest()->first();

        if($note){
            Note::create([
                'title' => $request->title,
                'content' => $request->content,
                'user_id' => auth()->user()->id,
                'color' => $request->color,
                'order' => $note->order + 1
            ]);
        }
        else {
            Note::create([
                'title' => $request->title,
                'content' => $request->content,
                'user_id' => auth()->user()->id,
                'color' => $request->color,
                'order' => 1
            ]);
        }

        return $this->success('Note created successfully!');
    }

    public function update(Request $request, $id)
    {
        $note = Note::find($id);
        if(empty($note)) return $this->error('Note id not found');
        
        $note->update([
            'title' => $request->title,
            'content' => $request->content,
            'color' => $request->color,
        ]);

        return $this->success('Note updated successfully'); 
    }

    public function updateOrder(Request $request){
        Note::where('user_id', auth()->user()->id)->delete();

        foreach($request->notes as $note){
            Note::create([
                'title' => $note['title'],
                'content' => $note['content'],
                'user_id' => auth()->user()->id,
                'color' => $note['color'],
                'order' => $note['order']
            ]);
        }
    }

    public function destroy($id)
    {
        Note::destroy($id);
        return $this->success('Note deleted successfully!');
    }
}
